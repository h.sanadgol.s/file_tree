<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FileTree extends Controller
{

    public function index_tree1(Request $request)
    {
        $link = asset('images/vendor/jquery.fancytree/dist/skin-lion/icons.gif');

        $array_data = [["title" => "<a target='_blank' href='$link'>Node1.jpg</a>", "key" => "1", "type" => "jpg"],
            ["title" => "Folder 2", "key" => "2", "folder" => true, "children" => [
                ["title" => "Node21.mp3", "key" => "3", "type" => "mp3", "href" => "$link"],
                ["title" => "Node22.mp4", "key" => "4", "type" => "mp4", "href" => "#"]
            ]]
        ];
        $json_data = json_encode($array_data);

        return view('tree1')
            ->with('data', $json_data);
    }

    public function index_tree2(Request $request)
    {
        return "index_tree2";
    }

    public function ajax_tree2(Request $request)
    {
        return "ajax_tree2";
    }
}
