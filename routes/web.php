<?php

use App\Http\Controllers\FileTree;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::controller(FileTree::class)->group(function () {
    Route::get('/tree1', 'index_tree1')->name('tree1');
    Route::get('/tree2', 'index_tree2')->name('tree2');
    Route::post('/ajaxt2', 'ajax_tree2')->name('ajaxt2');
});
